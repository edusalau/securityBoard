#ifndef __MAIN_BOARD_H
#define __MAIN_BOARD_H
#include <Arduino.h>

/*
 * MainBoard Class
 * Information relative of the Board as Name, Key and status
 */
class MainBoard {
  private:
    String nameBoard = "";
    String keyBoard = "";
    bool isConfigured = false;
    bool isActivated = false;
    bool isBluetooth = false;
  public:
    MainBoard() {};
    String getNameBoard() {
      return nameBoard;
    }
    String getKeyBoard() {
      return keyBoard;
    }
    bool getIsConfigured() {
      return isConfigured;
    }
    bool getIsActivated() {
      return isActivated;
    }
    bool getIsBluetooth() {
      return isBluetooth;
    }
    void setNameBoard(String nb) {
      nameBoard = nb;
    }
    void setKeyBoard(String kb) {
      keyBoard = kb;
    }
    void setIsConfigured(bool c) {
      isConfigured = c;
    }
    void setIsActivated(bool a) {
      isActivated = a;
    }
    void setIsBluetooth(bool b) {
      isBluetooth = b;
    }
    void print(){
      Serial.println("name"+nameBoard+" key"+keyBoard);
      Serial.println("ic"+(String)isConfigured+"ia"+(String)isActivated+"ib"+(String)isBluetooth);
    }
};

#endif