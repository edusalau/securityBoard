#ifndef __TIME_DHT_CONTROL_H
#define __TIME_DHT_CONTROL_H
#include <Arduino.h>
#include <TimeLib.h>
#include <SimpleDHT.h>
#include "LCDControl.h"
#include "BuzzerPlayer.h"

/*
 * TimeDHTControl controls the Clock Time of the Board and the DHT temperature and humidity values.
 * For the DHT is uses the SimpleDHT.h library and the pin 11 by default.
 * Checking the current time with TimeLib.h library, upgrade by minute the clock of the board and
 * the DHT values. After each upgrade it checks also the possible timers and activate them in case.
 */
class TimeDHTControl {
  private:
    SimpleDHT11 dht11;
    LCDControl* lcdControl;
    bool wakeup = false;
  public:
    TimeDHTControl(){
      dht11 = SimpleDHT11(11);
    };
    TimeDHTControl(LCDControl* lcdc, int pin) {
      lcdControl = lcdc;
      dht11 = SimpleDHT11(pin);
    };
    /* Obtain the DHT Values from SimpleDHT.h
     * @return byte* of two position {temperature, humidity}
    */
    byte * obtainDHTValues () {
      byte temperature = 0;
      byte humidity = 0;
      static byte result[2];
      int err = SimpleDHTErrSuccess;
      if ((err = dht11.read(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
        result[0], result[1] = -1000;
        return result;
      }
      result[0] = temperature;
      result[1] = humidity;
      // Serial.println("T"+(String) temperature + "C H"+ (String) humidity + "%");
      return result;
    }
    /* Obtain the String format of the DHT Values
     * @return String
    */
    String obtainTemperature() {
      byte * dhtValues = obtainDHTValues();
      if (dhtValues[0] == -1000) return "no signal";
      Serial.println("T"+(String) dhtValues[0] + "C H"+ (String) dhtValues[1] + "%");
      return "T"+(String) dhtValues[0] + "C H"+ (String) dhtValues[1] + "%";
    };
    /* Check current Time by TimeLib.h and upgrade the clock of the board and the backlight for inactive mode
     * @param SecurityBoardObject* sbo
     * @param bool showFirstTime = false
    */
    void updateTimeDHTControl(SecurityBoardObject* sbo, bool showFirstTime = false) {
      TimeBoard currentTime = TimeBoard((String) hour(), (String) minute());
      if(!sbo->getTimeBoard().compare(currentTime) || showFirstTime){
        String temperature = obtainTemperature();
        sbo->getTimeBoardRef()->setTimeBoard(currentTime);
        lcdControl->clear_LCD();
        if (sbo->getScreen().getBacklight().toInt() > 10) lcdControl->setBacklight_LCD(10);
        lcdControl->setScreenInsert_LCD(sbo->getTimeBoard().getTimeFormat()+" "+temperature, "(*)Alarm (#)Menu");
      }
    }
    /* Check current Time and Timers
     * @param SecurityBoardObject* sbo
     * @param BuzzerPlayer* buzzerPlayer
     * @param KeypadControl* keypadControl
     * @param bool showFirstTime = false
    */
    void stepTime(SecurityBoardObject* sbo, BuzzerPlayer* buzzerPlayer, KeypadControl* keypadControl, bool showFirstTime = false) {
      updateTimeDHTControl(sbo, showFirstTime);
      if (sbo->getWakeup().getIsActive() == true && sbo->getWakeup().compare(sbo->getTimeBoard()) && !wakeup) {
        buzzerPlayer->playMusic(LEYENDA, keypadControl, true);
        wakeup = true;       
      } else if (sbo->getReminder().getIsActive() == true && sbo->getReminder().compare(sbo->getTimeBoard())) {
        sbo->getReminderRef()->setIsActive(false);
        buzzerPlayer->playMusic(ALLA_TURCA, keypadControl, true);
      }
      if (wakeup && !sbo->getWakeup().compare(sbo->getTimeBoard())) {
        wakeup = false;
      }
    }
};
#endif