#include <Arduino.h>
#include "BuzzerPlayer.h"

const int totalMelodies = 4;
const int tempos[4] = {ALLEGRO, ALLEGRO, ADAGIO, LARGO};
const int lengths[4] = {49,38,40,66};
const int selections[4][2][66] = {{{
  NOTE_G3, NOTE_D4, NOTE_AS3, NOTE_D4, NOTE_DS4, NOTE_D4, NOTE_C4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_AS3, NOTE_D4, 
  NOTE_G3, NOTE_D4, NOTE_AS3, NOTE_D4, NOTE_DS4, NOTE_D4, NOTE_C4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_AS3, NOTE_D4,
  NOTE_G3, NOTE_D4, NOTE_AS3, NOTE_D4, NOTE_DS4, NOTE_D4, NOTE_C4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_AS3, NOTE_D4,
  NOTE_C4, NOTE_D4, NOTE_A3, NOTE_D4, NOTE_AS3, NOTE_D4, NOTE_C4, NOTE_D4, NOTE_A3, NOTE_D4, NOTE_AS3, NOTE_D4, NOTE_G3
},{
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16 
}},{{
  NOTE_B4, NOTE_A4, NOTE_GS4, NOTE_A4,
  NOTE_C5, NOTE_D5, NOTE_C5, NOTE_B4, NOTE_C5,
  NOTE_E5, NOTE_F5, NOTE_E5, NOTE_DS5, NOTE_E5,
  NOTE_B5, NOTE_A5, NOTE_GS5, NOTE_A5, NOTE_B5, NOTE_A5, NOTE_GS5, NOTE_A5,
  NOTE_C6, NOTE_A5, NOTE_C6,
  NOTE_B5, NOTE_A5, NOTE_G5, NOTE_A5,
  NOTE_B5, NOTE_A5, NOTE_G5, NOTE_A5,
  NOTE_B5, NOTE_A5, NOTE_G5, NOTE_FS5,
  NOTE_E5
},{
  16, 16, 16, 16,
  8, 16, 16, 16, 16,
  8, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16,
  4, 8, 8,
  8, 8, 8, 8,
  8, 8, 8, 8,
  8, 8, 8, 8,
  4
}},{{
  NOTE_FS2,
  NOTE_B3, NOTE_FS2, NOTE_B3, NOTE_D3, NOTE_B3,
  NOTE_D3, NOTE_B3, NOTE_D3, NOTE_FS3, NOTE_D3,
  NOTE_FS3, NOTE_D3, NOTE_FS3, NOTE_A4, NOTE_FS2,
  NOTE_D3, NOTE_FS2, NOTE_D3, NOTE_FS3,
  NOTE_A3,
  NOTE_D3, NOTE_A3, NOTE_D3, NOTE_FS3, NOTE_D3,
  NOTE_FS3, NOTE_D3, NOTE_FS3, NOTE_A4, NOTE_FS2,
  NOTE_A4, NOTE_FS3, NOTE_A3, NOTE_CS4, NOTE_CS3,
  NOTE_FS3, NOTE_CS3, NOTE_FS3, NOTE_AS3,
},{
  8,
  6, 16, 8, 3, 3,
  6, 16, 8, 3, 3,
  6, 16, 8, 3, 3,
  6, 16, 8, 2,
  8,
  6, 16, 8, 3, 3,
  6, 16, 8, 3, 3,
  6, 16, 8, 3, 3,
  6, 16, 8, 2,
}},{{
  NOTE_G4, NOTE_G4, NOTE_G4, NOTE_DS4, NOTE_AS4,
  NOTE_G4, NOTE_DS4, NOTE_AS4, NOTE_G4,
  NOTE_D5, NOTE_D5, NOTE_D5, NOTE_DS5, NOTE_AS4,
  NOTE_FS4, NOTE_DS4, NOTE_AS4, NOTE_G4,
  NOTE_G5, NOTE_G4, NOTE_G4, NOTE_G5, NOTE_FS5, NOTE_F5,
  NOTE_E5, NOTE_DS5, NOTE_E5, NOTE_GS4, NOTE_CS5, NOTE_C5, NOTE_B4,
  NOTE_AS4, NOTE_A4, NOTE_AS4, NOTE_DS4, NOTE_FS4, NOTE_DS4, NOTE_FS4,
  NOTE_AS4, NOTE_G4, NOTE_AS4, NOTE_D4,
  NOTE_G5, NOTE_G4, NOTE_G4, NOTE_G5, NOTE_FS5, NOTE_F5,
  NOTE_E5, NOTE_DS5, NOTE_E5, NOTE_GS4, NOTE_CS5, NOTE_C5, NOTE_B4,
  NOTE_AS4, NOTE_A4, NOTE_AS4, NOTE_DS4, NOTE_FS4, NOTE_DS4, NOTE_AS4,
  NOTE_G4, NOTE_DS4, NOTE_AS4, NOTE_G4
},{
  4, 4, 4, 6, 16,
  4, 6, 16, 2,
  4, 4, 4, 6, 16,
  4, 6, 16, 2,
  4, 6, 16, 4, 6, 16,
  16, 16, 8, 8, 4, 6, 16,
  16, 16, 8, 8, 4, 6, 16,
  4, 6, 16, 2,
  4, 6, 16, 4, 6, 16,
  16, 16, 8, 8, 4, 6, 16,
  16, 16, 8, 8, 4, 6, 16,
  4, 6, 16, 2   
}}};

BuzzerPlayer::BuzzerPlayer() {
}

BuzzerPlayer::BuzzerPlayer(int pin) {
  pinBuzzerPlayer = pin;
}

int BuzzerPlayer::getSelectionLength(int selection) {
  return lengths[selection];
}

void BuzzerPlayer::playMusic(int selection, KeypadControl* kp, bool stop = false) {
  if(selection >= 0 && selection < totalMelodies) { 
    String pad = "";
    bool keyPressed = false;
    int count = 0;
    do {
      for (int thisNote = 0; thisNote < lengths[selection]; thisNote++) {
        int noteDuration = tempos[selection] / selections[selection][1][thisNote];
        int pauseBetweenNotes = noteDuration * 1.3;
        tone(pinBuzzerPlayer, selections[selection][0][thisNote], noteDuration);
        delay(pauseBetweenNotes);
        noTone(pinBuzzerPlayer);
        delay(100);
        if(stop) {
          pad = kp->getKeyPressed();
          if (pad != "") {
            thisNote = lengths[selection];
            keyPressed = true;
          }
          count++;
        }
      }
    } while(stop && !keyPressed && count < 200);
  }
}

void BuzzerPlayer::alarmMusic(int thisNote, int selection = MARCHA_IMPERIAL) {
  int noteDuration = tempos[selection] / selections[selection][1][thisNote];
  int pauseBetweenNotes = noteDuration * 1.3;
  tone(pinBuzzerPlayer, selections[selection][0][thisNote], noteDuration);
  delay(pauseBetweenNotes);
  noTone(pinBuzzerPlayer);
  delay(100);
}

void BuzzerPlayer::beep() {
  int noteDuration = LARGO / 4;
  int pauseBetweenNotes = noteDuration * 1.3;
  tone(pinBuzzerPlayer, NOTE_C4, noteDuration);
  delay(pauseBetweenNotes);
  noTone(pinBuzzerPlayer);
  delay(100);
}
