#ifndef __SCREEN_BOARD_H
#define __SCREEN_BOARD_H
#include <Arduino.h>

/*
 * ScreenBoard Class
 * Information relative of the Screen LCD values when user manage the Board
 */
class ScreenBoard {
  private:
    String backlight = "80";
    String contrast = "50";
  public:
    ScreenBoard() {};
    String getBacklight() {
      return backlight;
    }
    String getContrast() {
      return contrast;
    }
    void setBacklight(String b) {
      backlight = b;
    }
    void setContrast(String c) {
      contrast = c;
    }
    void print(){
      Serial.println((String)backlight+"-"+(String)contrast);
    }
};

#endif