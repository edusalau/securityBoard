#ifndef __KEYPAD_CONTROL_H
#define __KEYPAD_CONTROL_H
#include <Arduino.h>
#include <Keypad.h>

// NUMBER OF ROWS AND COLS
const byte rows = 4;
const byte cols = 4;
// SYMBOLS of the KEYPAD
const char keyMap [rows] [cols] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};
// DEFAULT PINS FOR KEYPAD
const byte pin_rows [4] = {A6, A7, A8, A9};
const byte pin_cols [4] = {A10, A11, A12, A13};

/*
 * KeypadControl manage the Keypad keys using the Keypad.h library and expecting the Keypad 4x4.
 * The pins and keypad values above defined
 */
class KeypadControl {
  private:
    Keypad myKeypadControl = Keypad(makeKeymap(keyMap), pin_rows, pin_cols, rows, cols);
  public:
    KeypadControl() {};
    /* 
     * @param pinRows define Keypad rows pins.
     * @param pinCols define Keypad cols pins.
    */
    KeypadControl(byte *pinRows, byte *pinCols) {
      myKeypadControl = Keypad(makeKeymap(keyMap), pinRows, pinCols, rows, cols);
    }
    /*
     * Retrieve the key pressed
     * @return key pressed in String
    */
    String getKeyPressed() {
      char keypressed = myKeypadControl.getKey();
      return String(keypressed);
    }
    /* 
     * Check if the key presed is a number
     * @param pad the key pressed.
     * @return bool
    */
    bool checkNumbers (String pad) {
      return pad != "A" && pad != "B" && pad != "C" && pad != "D" && pad != "*" && pad != "#";
    }
        /* CONVERT CHARACTER (A,B,C,D) to INT
     * String pad
    */
    int characterToInt (String pad) {
      return pad == "A" ? 0 : pad == "B" ? 1 : pad == "C" ? 2 : 3;
    }
    /* CONVERT INT to CHAR (A,B,C,D)
     * String pad
    */
    String intToCharacter (int i) {
      return i == 0 ? "A" : i == 1 ? "B" : i == 2 ? "C" : "D";
    }
};

#endif
