/*
 * SecurityBoard is a project who works as security system for office and home.
 * The system is provided with a user interface by the LCD display that allows to
 * customize the system, by an intuitive menu controlled throw the Keypad.
 * It must need a key password but it also allows the use of physical cardkeys
 * manage by MFRC522.
 * The system provide a secure alarm that controlled two zones: triggered by 
 * UltraSonic and Movement Sensor. Ultrasonic is also upgraded by DHT Sensor, that
 * increase its precision.
 * In deactivad mode, the system will shows a clock and the temperature and humidity
 * mesaures by DHT Sensor. Also is included two reminders: wakeUp and reminder, with
 * the possibility to be reproduced daily or just once.
 */

#include <TimeLib.h>
#include <LiquidCrystal.h>
#include "Menu.h"
#include "SecurityBoardObject.h"
#include "LCDControl.h"
#include "TimeDHTControl.h"
#include "AlarmSensors.h"
#include <SPI.h>

// LCD Pins ---------- RS, E, D4, D5, D6, D7
LiquidCrystal _lcdInit(A0, A1, A2, A3, A4, A5);
// MFRC522 PINS SDA, RST --  Rest by SPI in MEGA2560 are preconfigurated to SCK=52 MOSI=51 MI=50  
MFRC522 _mfrc522(53, 5); 

const LCDControl lcdControl = LCDControl(&_lcdInit); // LCDControl default constructor pins VO=13 & A=12
KeypadControl keypadControl = KeypadControl(); // KeypadControl pins {A6, A7, A8, A9, A10, A11, A12, A13}; 
const TimeDHTControl timeDHT = TimeDHTControl(&lcdControl, 11);
BuzzerPlayer buzzerPlayer = BuzzerPlayer(); // Buzzplayer pin 10
AlarmSensors alarmSensors = AlarmSensors(&_mfrc522); // AlarmSensors default constructor Ultrasonic pins={9,8} Movement pin=7
SecurityBoardObject mainSBO = SecurityBoardObject(); // Information of the securityBoard
Menu menu = Menu(&mainSBO, &lcdControl, &keypadControl, &alarmSensors);

void setup() {
  Serial.begin(9600);
  analogWrite(12, 75); //set LCD some backlight on
  analogWrite(13 , 80); //set LCD some contrast
  pinMode(9, OUTPUT); // set UltraSonic trigger pin
  pinMode(8, INPUT); // set UltraSonic echo pin
  pinMode(7, INPUT); // set Movement HC-SR501 echo pin
  setTime(12, 0, 0, 1, 1, 2023);
  SPI.begin();			// Init SPI bus
	_mfrc522.PCD_Init();		// Init MFRC522
	delay(4);				// Optional delay. Some board do need more time after init to be ready, see Readme
  _lcdInit.begin(16, 2);
  lcdControl.setScreenInsert_LCD(init_0, init_1);
  buzzerPlayer.playMusic(WALKIRIA, &keypadControl);
  menu.runMenu();
  timeDHT.stepTime(&mainSBO, &buzzerPlayer, &keypadControl, true);
}

void loop() {
  if(!mainSBO.getMainBoard().getIsActivated()) {
    String pad = keypadControl.getKeyPressed();
    if(pad == "") {
      timeDHT.stepTime(&mainSBO, &buzzerPlayer, &keypadControl, false);
    } else {
      lcdControl.setBacklight_LCD(mainSBO.getScreen().getBacklight().toInt());
      if(pad == "#" || pad == "*") {
        menu.runMenu(pad == "#" ? MENU_LCD : ALARM_MENU);
      }
      timeDHT.stepTime(&mainSBO, &buzzerPlayer, &keypadControl, true);
      if(mainSBO.getMainBoard().getIsActivated()) {
        alarmSensors.activation(&timeDHT, &lcdControl, &buzzerPlayer);
      }
    }
  } else {
    alarmSensors.controlAlarm(&timeDHT, &mainSBO, &keypadControl, &lcdControl, &buzzerPlayer);
    timeDHT.stepTime(&mainSBO, &buzzerPlayer, &keypadControl, true);
  }
}
