/*
 * SecurityBoard is a project who works as security system for office and home.
 * The system is provided with an user interface by the LCD display that allows to
 * customize the system, by an intuitive menu controlled throw the Keypad.
 * It must need a key password but it also allows the use of physical cardkeys
 * manage by MFRC522.
 * The system provide a secure alarm that controlled two zones: triggered by 
 * UltraSonic and Movement Sensor. Ultrasonic is also upgraded by DHT Sensor, that
 * increase its precision.
 * In deactivad mode, the system will shows a clock and the temperature and humidity
 * mesaures by DHT Sensor. Also is included two Reminders: wakeUp and reminder, with
 * the possibility to be reproduced daily or just once.
 * Alarm and Reminders reproduced a different melody by passive buzzer.
 *
 * ----------------------------------------------------------------------------------
 * -------------------------------  INSTALLATION  -----------------------------------
 * ----------------------------------------------------------------------------------
 * Arduino Elegoo Mega 2560
 * LCD Display 1602         ==> VO=13 A=12, {RS, E, D4, D5, D6, D7}=(A0, A1, A2, A3, A4, A5)
 * Keypad 4x4               ==> {A6, A7, A8, A9, A10, A11, A12, A13}
 * UltraSonic HCRS04 Sensor ==> trigger=9 echo=8
 * Movement Sensor          ==> 7
 * Passive buzzer           ==> 10
 * MFRC522                  ==> RST=5 SDA=53 SCK=52 MOSI=51 MI=50
 * DHT11 Sensor             ==> 11
 *
 * Download & Compile by Arduino IDE
 *
 */
