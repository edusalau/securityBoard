#ifndef __ALARM_SENSORS_H
#define __ALARM_SENSORS_H
#include <Arduino.h>
#include "BuzzerPlayer.h"
#include "TimeDHTControl.h"
#include <MFRC522.h>

/*
 * AlarmSensors controls the Ultrasonic & Movement Sensors, the CardReader HC522,
 * and the Alarm itself. By default trigger and echo pins for UltraSonic are 9 and 8 respectivily
 * and the pin 7 for Movement and the MFRC522 by 5 RST and 53 SDA (52 51 50 rest).
 * For the Ultrasonic is expected to be improved with DHT temperature and humidity values.
 * During the process of Alarm activation and when the Alarm detect the danger,
 * it will control the lcd messages, call the buzzerPlayer and manage the keyPad
 */
class AlarmSensors {
  private:
    int pinTriggerUltraSonic = 9;
    int pinEchoUltraSonic = 8;
    float distanceUltraSonic = 0;
    int pinMovement = 7;
    MFRC522* mfrc522;
    /*
     * Given dht temperature and humidity values obtain the ultrasonic distance
     * and retrieve if has being changed
     * @param byte dhtValues[2]
     * @return bool
     */
    bool checkDistance(byte dhtValues[2]) {
      // Calculate speed of sound in m/s 331.4*(0.6*temp)+(0.0124*hum)
      float speedOfSound = 331.4 + (0.6 * dhtValues[0]) + (0.0124 * dhtValues[1]);
      // Send UltraSonic
      digitalWrite(pinTriggerUltraSonic, LOW);
      delayMicroseconds(2);
      digitalWrite(pinTriggerUltraSonic, HIGH);
      delayMicroseconds(10);
      digitalWrite(pinTriggerUltraSonic, LOW);
      // Obtain duration Ultrasonic
      float duration = pulseIn(pinEchoUltraSonic, HIGH);
      duration = duration/1000000; // Convert mircroseconds to seconds
      // Manage duration to obtain distance
      float distance = (speedOfSound * duration)/2;
      distance = distance * 100; // meters to centimeters
      Serial.println((String) distance + "vs"+ (String) distanceUltraSonic);
      if (distance < distanceUltraSonic-20 || distance > distanceUltraSonic+20) {
        if (distanceUltraSonic == 0) {
          distanceUltraSonic = distance;
          return false;
        } else {
          return true;
        }
      }
      distanceUltraSonic = distance;
      return false;
    };
    /*
     * Retrieve if has detected a movement
     * @return bool
     */
    bool checkMovement() {
      if(digitalRead(pinMovement) == HIGH) {
        Serial.println("Movimiento detectado");
        return true;
      }
      Serial.println("No one");
      return false;
    };
    /*
     * Retrieve if has detected a danger
     * @param byte dhtValues[2]
     * @return bool
     */
    bool checkDanger(byte dhtValues[2]) {
      return checkDistance(dhtValues) || checkMovement();
    };
  public:
    AlarmSensors(){};
    AlarmSensors(MFRC522* _mfrc522){
      mfrc522 = _mfrc522;
    };
    AlarmSensors(int pinTUS, int pinEUS, int pinMS) {
      pinTriggerUltraSonic = pinTUS;
      pinEchoUltraSonic = pinEUS;
      pinMovement = pinMS;
    };
    /*
     * Activate the Alarm
     * @param TimeDHTControl timeDHT
     * @param LCDControl lcdControl
     * @param BuzzerPlayer bp
     */
    void activation(TimeDHTControl* timeDHT, LCDControl* lcdControl, BuzzerPlayer* bp) {
      distanceUltraSonic = 0;
      lcdControl->setScreenInsert_LCD("-ALARM          ","  IS ACTIVATING-");
      for (int i=5; i>0;i--){
        bp->beep();
        lcdControl->setScreenInsert_LCD("-ALARM will be  "," in "+(String)i);
      }
      bool check = checkDistance(timeDHT->obtainDHTValues());
      lcdControl->setScreenInsert_LCD("-ALARM          ","   IS ACTIVATED-");
    };
    /*
     * Read a cardKey and save it in memory argument
     * @param MFRC522::Uid* cardKey memory
     * @return bool flag if save it a card
     */
    bool saveCardKey(MFRC522::Uid* cardKey) {
      if ( mfrc522->PICC_IsNewCardPresent()) {  
        if ( mfrc522->PICC_ReadCardSerial()) {
          Serial.print("Card UID:");
          cardKey->size = mfrc522->uid.size;
          cardKey->sak = mfrc522->uid.sak;
          for (byte i = 0; i < mfrc522->uid.size; i++) {
            cardKey->uidByte[i] = mfrc522->uid.uidByte[i];

            Serial.print(mfrc522->uid.uidByte[i] < 0x10 ? " 0" : " ");
            Serial.print(mfrc522->uid.uidByte[i], HEX);   
          } 
         Serial.println();
          mfrc522->PICC_HaltA();
          Serial.print("CardKEY UID:");
          for (byte i = 0; i < cardKey->size; i++) {
            Serial.print(cardKey->uidByte[i] < 0x10 ? " 0" : " ");
            Serial.print(cardKey->uidByte[i], HEX);   
          }
          return true;     
        }      
	    }
      return false;
    };
    /*
     * Read a cardKey and compare it with one by argument
     * @param MFRC522::Uid cardKey
     * @return bool flag if is the same card
     */
    bool compareCardKey(MFRC522::Uid cardKey) {
      if ( mfrc522->PICC_IsNewCardPresent()) {  
        if ( mfrc522->PICC_ReadCardSerial()) {
          Serial.print("Card UID:");
          bool check = true;
          for (byte i = 0; i < mfrc522->uid.size && check; i++) {
            if (mfrc522->uid.uidByte[i] != cardKey.uidByte[i]) check = !check;
            Serial.print(mfrc522->uid.uidByte[i] < 0x10 ? " 0" : " ");
            Serial.print(mfrc522->uid.uidByte[i], HEX);   
          } 
         Serial.println("Compared="+(String)check);
          mfrc522->PICC_HaltA();
          return check;
	    }}
      return false;
    };
    /*
     * Control the Alarm
     * @param TimeDHTControl timeDHT
     * @param SecurityBoardObject* sbor
     * @param KeypadControl kp
     * @param LCDControl lcdControl
     * @param BuzzerPlayer bp
     */
    void controlAlarm(TimeDHTControl* timeDHT, SecurityBoardObject* sbor, KeypadControl* keypadControl, LCDControl* lcdControl, BuzzerPlayer* buzzerPlayer) {
      byte * dhtValues = timeDHT->obtainDHTValues();
      sbor->setIsDanger(checkDanger(dhtValues));
      if(sbor->getIsDanger()) {
        lcdControl->setBacklight_LCD(sbor->getScreen().getBacklight().toInt());
        lcdControl->setScreenInsert_LCD("-WARNING!!!     ","   ENTER KEY   -", 750);
        lcdControl->setScreenInsert_LCD("#=Verify *//*D=DEL","Key: ", 0);
        String pad;
        String pass = "";
        String passHide = "";
        int thisNote = 0;
        bool checkKey = false;
        do {
          buzzerPlayer->alarmMusic(thisNote);
          thisNote++;
          thisNote = thisNote < buzzerPlayer->getSelectionLength(MARCHA_IMPERIAL) ? thisNote : 0;
          lcdControl->setScreenInsert_LCD("#=Verify *//*D=DEL","Key: "+passHide, 0);
          pad = keypadControl->getKeyPressed();
          if((pad == "*" || pad == "D") && pass.length() > 0) {
            pass.remove(pass.length()-1);
            passHide.remove(passHide.length()-1);
          } else if (pad == "#" && pass != sbor->getMainBoard().getKeyBoard()) {
            pass = "";
            passHide = "";
            pad = "";
            lcdControl->setScreenInsert_LCD("-WRONG KEY!!!","  ENTER AGAIN  -", 750);
          } else if(pad != "") {
            pass += pad;
            passHide += "*";
          }
          checkKey = compareCardKey(sbor->getCardKey());
        } while ((pad != "#" && pass != sbor->getMainBoard().getKeyBoard()) && !checkKey);
        sbor->setIsDanger(false);
        sbor->getMainBoardRef()->setIsActivated(false);
      }
    };
};
#endif
