#include <Arduino.h>
#include "Menu.h"

const String options_MENU [5][4] = {
 {M_BOARD, M_TIME, M_SCREEN, M_CARDKEY},
 {M_NAME, M_KEY},
 {M_HOUR, M_MINUTE},
 {M_BACKLIGHT, M_CONTRAST},
 {M_CARDKEY},
};

const String optionsMenuMessagesMap [5] [2] = {
  {menu_0, menu_1},
  {boar_0, boar_1},
  {time_0, time_1},
  {scre_0, scre_1},
  {blue_0, remi_1},
};

const String options_ALARM [4][3] = {
 {A_ALARM, A_WAKEUP, A_REMINDER},
 {A_ALARM},
 {A_WAKEUP_H, A_WAKEUP_M},
 {A_REMINDER_H, A_REMINDER_M},
};

const String optionsAlarmMessagesMap [4] [2] = {
  {alme_0, alme_1},
  {remi_0, remi_1},
  {wurm_0, time_1},
  {wurm_0, time_1},
};

Menu::Menu(SecurityBoardObject* sbo, LCDControl* lcdc, KeypadControl* kc, AlarmSensors* as) {
  menuSBO = sbo;
  lcdControl = lcdc;
  keypadControl = kc;
  alarmSensors = as;
}

bool Menu::pressBack (String pad) {
  return pad == "*";
}

bool Menu::pressNext (String pad) {
  return pad == "#";
}

bool Menu::checkMenuTime(String optionMenu) {
  return optionMenu == M_HOUR || optionMenu == A_WAKEUP_H || optionMenu == A_REMINDER_H || optionMenu == M_MINUTE || optionMenu == A_WAKEUP_M || optionMenu == A_REMINDER_M;
}

bool Menu::activeDeactiveOptions(int optionMenu, int menu) {
  return (menu == MENU_LCD && optionMenu == MENU_CARDKEY) || (menu == ALARM_MENU && optionMenu == ALARM_ALARM);
}

bool Menu::validateConfiguration(int optionMenu) {
  bool validation = true;
  if (optionMenu == MENU_BOARD) validation = menuSBO->getMainBoard().getNameBoard() != "" && menuSBO->getMainBoard().getKeyBoard() != "";
  if (!validation) {
    lcdControl->setScreenInsert_LCD(conf_0, missC1, 2000);
  }
  return validation;
}

String Menu::getOption(String pad, int menu, int mainMenu) {
  return mainMenu == MENU_LCD ? options_MENU[menu][keypadControl->characterToInt(pad)] : options_ALARM[menu][keypadControl->characterToInt(pad)];
}

void Menu::saveOption(String option, String transOption, String messageOption) {
  Serial.println("SO1"+option+"-"+transOption);
  Serial.println("SO2"+messageOption);
  if(transOption == M_NAME) menuSBO->getMainBoardRef()->setNameBoard(option);
  else if(transOption == M_KEY)menuSBO->getMainBoardRef()->setKeyBoard(option);
  else if(transOption == A_ALARM) menuSBO->getMainBoardRef()->setIsActivated(option == "A");
  else if(transOption == M_HOUR || transOption == M_MINUTE){
    if(transOption == M_HOUR) menuSBO->getTimeBoardRef()->setHourBoard(option);
    else if(transOption == M_MINUTE) menuSBO->getTimeBoardRef()->setMinuteBoard(option);
    setTime(menuSBO->getTimeBoardRef()->getHourBoard().toInt(),
      menuSBO->getTimeBoardRef()->getMinuteBoard().toInt(),0,1,1,2023);
  } else if(transOption == M_BACKLIGHT) {
    menuSBO->getScreenRef()->setBacklight(option);
    lcdControl->setBacklight_LCD(option.toInt());
  } else if(transOption == M_CONTRAST) {
    menuSBO->getScreenRef()->setContrast(option);
    lcdControl->setContrast_LCD(option.toInt());
  } else if(transOption == A_WAKEUP_M || transOption == A_WAKEUP_H) {
    if(transOption == A_WAKEUP_H) menuSBO->getWakeupRef()->setHourBoard(option);
    else if(transOption == A_WAKEUP_M) menuSBO->getWakeupRef()->setMinuteBoard(option);
    menuSBO->getWakeupRef()->setIsActive(true);
  } else if(transOption == A_REMINDER_M || transOption == A_REMINDER_H) {
    if(transOption == A_REMINDER_H) menuSBO->getReminderRef()->setHourBoard(option);
    else if(transOption == A_REMINDER_M) menuSBO->getReminderRef()->setMinuteBoard(option);
    menuSBO->getReminderRef()->setIsActive(true);
  }
  lcdControl->setScreenInsert_LCD(saved_, messageOption + option);
}

String Menu::validateOption(String modifiedOption, int menu, String optionMenu) {
  String validation = modifiedOption;
  if(checkMenuTime(optionMenu)){
    if((optionMenu == M_HOUR || optionMenu == A_WAKEUP_H || optionMenu == A_REMINDER_H) && validation.toInt() > 23) {
      validation = "23";          
    } else if((optionMenu == M_MINUTE || optionMenu == A_WAKEUP_M || optionMenu == A_REMINDER_M) && validation.toInt() > 59) {
      validation = "59"; 
    }
  } else {
    if(optionMenu == M_BACKLIGHT && validation.toInt() > 255) {
      validation = "255";          
    } else if(optionMenu == M_CONTRAST && validation.toInt() > 150) {
      validation = "150"; 
    }
  }
  return validation;
}

String Menu::modifyOption(String option, String pad, int menu, String optionMenu) {
  String modifiedOption = option;
  if(menu == MENU_BOARD){
    if(modifiedOption.length() < 10) {
      modifiedOption.concat(pad);
    } else {
      modifiedOption.remove(9);
      modifiedOption.concat(pad);
    }
  } else if((menu == MENU_SCREEN || checkMenuTime(optionMenu)) && keypadControl->checkNumbers(pad)) {
    int checkLength = checkMenuTime(optionMenu) ? 2 : 3;
    if(modifiedOption.length() < checkLength) {      
      modifiedOption.concat(pad);
      modifiedOption = validateOption(modifiedOption, menu, optionMenu);   
    } else {
      modifiedOption.remove(checkLength-1);
      modifiedOption.concat(pad);            
    }
  }
  return modifiedOption;
}

void Menu::setOption(String selectedOption, int menu, int mainMenu, bool isKey = false) {
  String transOption = getOption(selectedOption, menu, mainMenu);
  String messageOption = transOption + ": ";
  lcdControl->setScreenInsert_LCD(insert, messageOption);
  String option = "";
  String hideOption = "";
  String pad = "";
  bool checkKey = false; 
  do {
    pad = keypadControl->getKeyPressed();
    if(!pressBack(pad) && !pressNext(pad) && pad != "") {
      if(pad == "D" && option.length() > 0) {
        option.remove(option.length()-1);
        if(isKey) hideOption.remove(hideOption.length()-1);
      } else if(pad != "D") {
        option = modifyOption(option, pad, menu, transOption);
        if(isKey && option.length() < 10){
          hideOption.concat("*");
        }
      }
      if(!isKey){
        lcdControl->setScreenInsert_LCD(insert, messageOption + option);
      } else {
        String introducedKey = hideOption.length() > 1 ? hideOption.substring(0,hideOption.length()-1) : "";
        introducedKey += option.length() > 0 ? option.substring(option.length()-1, option.length()) : "";
        lcdControl->setScreenInsert_LCD(insert, messageOption + introducedKey);
        lcdControl->setScreenInsert_LCD(insert, messageOption + hideOption, 200);
      }
    }
    if (pad == "" && isKey) {
      checkKey = alarmSensors->compareCardKey(menuSBO->getCardKey());
      if (checkKey) pad = "#";
    }
  } while (!pressBack(pad) && !pressNext(pad));
  if (!isKey) {
    if (pressNext(pad)) {
      saveOption(option, transOption, messageOption);      
    }
  } else {
    if (pressBack(pad)) {
      endMenu = true;
    } else if (checkKey || option == menuSBO->getMainBoard().getKeyBoard()) {
      lcdControl->setScreenInsert_LCD(blank_, corre1);
      endMenu = false;
    } else {
      lcdControl->setScreenInsert_LCD(wrong0, wrong1);
      setOption("B", MENU_BOARD, mainMenu, true);      
    }
  }
}

void Menu::showCurrentOption(String pad, int optionMenu, int mainMenu) {  
  String option = getOption(pad, optionMenu, mainMenu);
  if(option != M_KEY) {
    String message0 = option +": ";
    String message1 = blank_;
    if(option == M_NAME) message0 += menuSBO->getMainBoard().getNameBoard();
    else if(option == M_CARDKEY) message0 = menuSBO->getCardKey().size > 0 ? "CardKey: ACTIVATED" : "CardKey: OFF";
    else if(option == A_ALARM) message0 = menuSBO->getMainBoard().getIsActivated() ? "Alarm: ACTIVATED" : "Alarm: OFF";
    else if(option == M_HOUR) message0 += menuSBO->getTimeBoard().getHourBoard();
    else if(option == M_MINUTE)  message0 += menuSBO->getTimeBoard().getMinuteBoard();
    else if(option == M_BACKLIGHT) message0 += menuSBO->getScreen().getBacklight();
    else if(option == M_CONTRAST) message0 += menuSBO->getScreen().getContrast();
    else if(option == A_WAKEUP_H) message0 += menuSBO->getWakeup().getHourBoard();
    else if(option == A_WAKEUP_M) message0 += menuSBO->getWakeup().getMinuteBoard();
    else if(option == A_REMINDER_H) message0 += menuSBO->getReminder().getHourBoard();
    else if(option == A_REMINDER_M) message0 += menuSBO->getReminder().getMinuteBoard();
    lcdControl->setScreenInsert_LCD(message0, blank_);    
  }
}

int Menu::runOptionMenu(int optionMenu, int menu) {
  Serial.println("RO-"+(String)optionMenu+"-"+(String)menu);
  if(activeDeactiveOptions(optionMenu, menu)){
    showCurrentOption("A", optionMenu, menu);     
  }
  if (menu == MENU_LCD) {
    lcdControl->setScreenInsert_LCD(optionsMenuMessagesMap[optionMenu][0], optionsMenuMessagesMap[optionMenu][1]);
  }else {
    lcdControl->setScreenInsert_LCD(optionsAlarmMessagesMap[optionMenu][0], optionsAlarmMessagesMap[optionMenu][1]);
  }
  String pad = "";
  do {
    pad = keypadControl->getKeyPressed();
    if (!activeDeactiveOptions(optionMenu, menu) && (pad == "A" || pad == "B"))  {
      showCurrentOption(pad, optionMenu, menu);
      setOption(pad, optionMenu, menu);
      if (menu == MENU_LCD) {
        lcdControl->setScreenInsert_LCD(optionsMenuMessagesMap[optionMenu][0], optionsMenuMessagesMap[optionMenu][1]);
      } else {
        lcdControl->setScreenInsert_LCD(optionsAlarmMessagesMap[optionMenu][0], optionsAlarmMessagesMap[optionMenu][1]);
      }
      pad = "";
    } else if (activeDeactiveOptions(optionMenu, menu) && menu != MENU_LCD && (pad == "A" || pad == "#")) {
      String message = A_ALARM;
      saveOption(pad, message, (pad == "A" ? message : "DEACTIVATED") + ": ");
      pad = "#";
    } else if (activeDeactiveOptions(optionMenu, menu) && menu == MENU_LCD) {
      if (pad != "#") {
        bool settedCard = alarmSensors->saveCardKey(menuSBO->getCardKeyRef());
        if (settedCard) {
          lcdControl->setScreenInsert_LCD("-----SAVED-----", "-----CARDKEY----");
          pad = "#";
        } else if(pad == "#") {
          lcdControl->setScreenInsert_LCD("-------NO------", "-----CARDKEY----");
        }
      } 
    }
  } while (pad != "*" && pad != "#");
  if (!validateConfiguration(optionMenu)) return runOptionMenu(optionMenu, menu);
  return pad == "*" ? -1 : 1; 
}

void Menu::configurationMenu() {
  int next = 0;
  for (int iteratorMenu=1; iteratorMenu<5; ) {
    next = runOptionMenu(iteratorMenu, MENU_LCD);  
    iteratorMenu = (iteratorMenu + next) == 0 ? 1 : iteratorMenu + next;
  }
}

void Menu::selectionMenu(int menu) {
  String pad = "";
  do {
    pad = keypadControl->getKeyPressed();
    if (pad == "A" || pad == "B" || pad == "C" || (menu == MENU_LCD && pad == "D"))  {
       if(menu == ALARM_MENU) {
        if (pad == "B") {
          lcdControl->setScreenInsert_LCD(dewu_0, menuSBO->getWakeup().getIsActive() ? acwr_1 : dewr_1);          
        } else if(pad == "C") {
          lcdControl->setScreenInsert_LCD(dere_0, menuSBO->getReminder().getIsActive() ? acwr_1 : dewr_1);      
        }
      }
      int back = runOptionMenu(1+keypadControl->characterToInt(pad), menu);
      if(menu == ALARM_MENU) {
        if (pad == "B") {
          menuSBO->getWakeupRef()->setIsActive(back != -1);
          lcdControl->setScreenInsert_LCD(dewu_0, back != -1 ? acwr_1 : dewr_1);          
        } else if(pad == "C") {
          menuSBO->getReminderRef()->setIsActive(back != -1);
          lcdControl->setScreenInsert_LCD(dere_0,  back != -1 ? acwr_1 : dewr_1);      
        }
      }
      if (menu == ALARM_MENU && pad == "A" && menuSBO->getMainBoard().getIsActivated()) {
        pad = "#";
      } else {
        lcdControl->setScreenInsert_LCD(menu == 0 ? menu_0 : alme_0, menu == 0 ? menu_1 : alme_1);
        pad = "";
      }
    }
  } while (pad != "*" && pad != "#");
}

void Menu::runMenu(int menu = 0) {
  lcdControl->setScreenInsert_LCD(menuSBO->getMainBoard().getIsConfigured() ? menu == 0 ? cmen_0 : cala_0 : conf_0 , cmen_1, 2500);
  lcdControl->setScreenInsert_LCD(p_back, p_next, 2500);
  if (menuSBO->getMainBoard().getIsConfigured()) {
    /* FIRST MUST INSERT THE KEY */    
    setOption("B", MENU_BOARD, MENU_LCD, true);
    if (!endMenu) {
      lcdControl->setScreenInsert_LCD(menu == 0 ? menu_0 : alme_0, menu == 0 ? menu_1 : alme_1, 2000);
      selectionMenu(menu);      
    }
    endMenu = false;
  } else {
    configurationMenu();
    menuSBO->getMainBoardRef()->setIsConfigured(true);
  }
  lcdControl->setScreenInsert_LCD(menuSBO->getMainBoard().getIsConfigured() ? menu == 0 ? cmen_0 : cala_0 : conf_0 , endm_1, 2500);
  lcdControl->clear_LCD();
}