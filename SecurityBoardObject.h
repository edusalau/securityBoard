#ifndef __SECURITY_BOARD_OBJECT_H
#define __SECURITY_BOARD_OBJECT_H
#include <Arduino.h>
#include "MainBoard.h"
#include "TimeBoard.h"
#include "ScreenBoard.h"
#include <MFRC522.h>

/*
 * SecurityBoardObject Class
 * Information relative of the Board: Main, Screen, Clock Time, Timers and Danger Status.
 */
class SecurityBoardObject {
  private:
    MainBoard mainBoard;
    ScreenBoard screen;
    TimeBoard timeBoard;
    TimeBoard wakeup;
    TimeBoard reminder;
    bool danger = false;
    MFRC522::Uid cardKey;
  public:
    SecurityBoardObject() {};
    MainBoard getMainBoard() {
      return mainBoard;
    }
    MainBoard* getMainBoardRef() {
      return &mainBoard;
    }
    ScreenBoard getScreen() {
      return screen;
    }
    ScreenBoard* getScreenRef() {
      return &screen;
    }
    TimeBoard getTimeBoard() {
      return timeBoard;
    }
    TimeBoard* getTimeBoardRef() {
      return &timeBoard;
    }
    TimeBoard getWakeup() {
      return wakeup;
    }
    TimeBoard* getWakeupRef() {
      return &wakeup;
    }
    TimeBoard getReminder() {
      return reminder;
    }
    TimeBoard* getReminderRef() {
      return &reminder;
    }
    bool getIsDanger() {
      return danger;
    }
    MFRC522::Uid getCardKey(){
      return cardKey;
    }
    MFRC522::Uid* getCardKeyRef(){
      return &cardKey;
    }
    void setCardKey(MFRC522::Uid uid) {
      cardKey = uid;
    }
    void setMainBoard(MainBoard mb) {
      mainBoard = mb;
    }
    void setScreen(ScreenBoard s) {
      screen = s;
    }
    void setTimeBoard(TimeBoard tb) {
      timeBoard = tb;
    }
    void setWakeup(TimeBoard tb) {
      wakeup = tb;
    }
    void setReminder(TimeBoard tb) {
      reminder = tb;
    }
    void setIsDanger(bool d) {
      danger = d;
    }
    void print(){
      Serial.print("M");mainBoard.print();
      Serial.print("S");screen.print();
      Serial.print("T");timeBoard.print();
      Serial.print("W");wakeup.print();
      Serial.print("R");reminder.print();
      Serial.println("Danger"+(String)danger);
    }
};

#endif