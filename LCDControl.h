#ifndef __LCD_CONTROL_H
#define __LCD_CONTROL_H
#include <Arduino.h>
#include <LiquidCrystal.h>

/*
 * LCDControl manage the LCD screen using the LiquidCrystal.h.
 * Object LiquidCrystal provided in the constructor will have the pins already defined.
 * By default LCD Pins {RS, E, D4, D5, D6, D7} = (A0, A1, A2, A3, A4, A5)
 * For Backlight and Contrast by default LCDControl will have pins 12 and 13.
 * It will keep the current Backlight information to avoid unnecessary refreshing in inactive mode.
 */
class LCDControl {
  private:
    LiquidCrystal* lcd;
    int backlightPin = 12;
    int contrastPin = 13;
    int currentBacklight = 80;
  public:
    LCDControl(){};
    LCDControl(LiquidCrystal* _lcd){
      lcd = _lcd;
    }
    /* CLEAN THE LCD SCREEN DISPLAY */
    void clear_LCD() {
      lcd->clear();
    }
    /* SET THE CONTRAST OF THE LCD SCREEN
     * int contrast = value between 0 and 150 - USE PWM Pin for analog write
     */
    void setContrast_LCD (int contrast) {
      analogWrite(contrastPin, contrast > 150 ? 150 : contrast);
      clear_LCD();
      lcd->setCursor(0, 1);
      lcd->print("CHANGED "+contrast);
      delay(200);
    }
    /* SET THE BACKLIGHT OF THE LCD SCREEN
     * int backlight = value between 0 and 255 - USE PWM Pin for analog write
    */
    void setBacklight_LCD (int backlight) {
      int mBacklight = backlight > 255 ? 255 : backlight;
      if (currentBacklight != mBacklight) {
        currentBacklight = mBacklight;
        analogWrite(backlightPin, currentBacklight);
        clear_LCD();
        lcd->setCursor(0, 1);
        //lcd->print("CHANGED "+currentBacklight);
        delay(200);
      }
    }
    /* INSERT VALUE SCREEN
     * String value to show, int row where display, bool withDelay if needs to delay
     */
    void showInsert_LCD (String value, int row, bool withDelay = false, int timeToDelay = 1500) {
      lcd->setCursor(0,row);
      lcd->print(value);
      if (withDelay) {
        delay(timeToDelay);    
      }
    }
    /* SET SCREEN
     * String value0 for row0
     * String value1 for row1
     */
    void setScreenInsert_LCD (String value0, String value1, int timeToDelay = 1500) {
      clear_LCD();
      showInsert_LCD (value0, 0);
      showInsert_LCD (value1, 1, true, timeToDelay);
    }
};

#endif
