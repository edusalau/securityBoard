#ifndef __TIME_BOARD_H
#define __TIME_BOARD_H
#include <Arduino.h>

/*
 * TimeBoard Class
 * Information relative of a Time with Timer Status
 */
class TimeBoard {
  private:
    String hourBoard = "0";
    String minuteBoard = "0";
    bool isActive = false;
  public:
    TimeBoard() {};
    TimeBoard(String hour, String minute) {
      hourBoard = hour;
      minuteBoard = minute;
    };
    String getHourBoard() {
      return hourBoard;
    }
    String getMinuteBoard() {
      return minuteBoard;
    }
    bool getIsActive() {
      return isActive;
    }
    void setTimeBoard(TimeBoard tb) {
      hourBoard = tb.getHourBoard();
      minuteBoard = tb.getMinuteBoard();
    }
    void setHourBoard(String hb) {
      hourBoard = hb;
    }
    void setMinuteBoard(String mb) {
      minuteBoard = mb;
    }
    void setIsActive(bool a) {
      isActive = a;
    }
    bool compare(TimeBoard tb) {
      return hourBoard == tb.getHourBoard() && minuteBoard == tb.getMinuteBoard();
    }
    String clockNumber(String number) {
      return number.toInt() >= 10 ? number : "0" + number;
    }
    String getTimeFormat() {
      return "H"+clockNumber(hourBoard)+":"+clockNumber(minuteBoard);
    }
    void print(){
      Serial.println(hourBoard+"-"+minuteBoard+"iA"+(String)isActive);
    }
};

#endif